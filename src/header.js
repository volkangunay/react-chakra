import React from "react";
import { Box, Heading, Flex, Text, Button, Icon } from "@chakra-ui/core";

const MenuItems = ({ children }) => (
    <Text mt={{ base: 4, md: 0 }} mr={6} display="block">
        {children}
    </Text>
);

const Header = props => {
    const [show, setShow] = React.useState(false);

    return (
        <Flex
            as="nav"
            align="center"
            justify="space-between"
            wrap="wrap"

            paddingRight="6rem"
            paddingTop="1rem"
            paddingBottom="1rem"
            paddingLeft="6rem"

            borderBottom="1px"
            color="#3F454E"

            bg="rgb(26, 32, 44)"
            {...props}
        >
            <Flex align="center" mr={5}>
                <Heading size="lg" color="white" letterSpacing={"-.1rem"}>
                    <Text>chakra</Text>
                </Heading>
            </Flex>

            <Box
                display={{ sm: show ? "block" : "none", md: "flex" }}
                width={{ sm: "full", md: "auto" }}
                alignItems="center"
                flexGrow={1}
                color="white"
                marginLeft="1.5rem"
            >
                <MenuItems>
                    <Button bg="#EAEDF3" size="xs" color="#A1A1A1" padding="0.8rem" borderRight="1px" borderBottomRightRadius="0" borderBottomLeftRadius="5" borderTopLeftRadius="5" borderTopRightRadius="0">
                        <Icon name="star" fontSize="sm" color="black" />
                        <Text fontSize="xs" marginLeft="0.4rem" color="black">Star</Text>
                    </Button>
                    <Button bg="#FFFFFF" size="xs" color="black" padding="0.8rem" borderBottomRightRadius="5" borderBottomLeftRadius="0" borderTopLeftRadius="0" borderTopRightRadius="5">
                        <Text fontSize="xs">10.362</Text>
                    </Button>
                </MenuItems>
            </Box>

            <Box
                display={{ sm: show ? "block" : "none", md: "flex" }}
                width={{ sm: "full", md: "auto" }}
                alignItems="center"
                flexGrow={1}
                color="white"
                marginLeft="1.5rem"
            >
                <MenuItems>

                </MenuItems>
            </Box>

            <Box
                display={{ sm: show ? "block" : "none", md: "block" }}
                mt={{ base: 4, md: 0 }}
            >
                <Button bg="#f5f5f5">
                    <svg stroke="#E53E3F" fill="#E53E3F" stroke-width="0" viewBox="0 0 512 512" class="css-1gaoxp0" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M462.3 62.6C407.5 15.9 326 24.3 275.7 76.2L256 96.5l-19.7-20.3C186.1 24.3 104.5 15.9 49.7 62.6c-62.8 53.6-66.1 149.8-9.9 207.9l193.5 199.8c12.5 12.9 32.8 12.9 45.3 0l193.5-199.8c56.3-58.1 53-154.3-9.8-207.9z"></path></svg>
                    <Text paddingLeft="0.4rem" color="black" fontSize="sm" >Sponsor</Text>
                </Button>
            </Box>


            <Box display={{ sm: show ? "block" : "none", md: "block" }}
                mt={{ base: 4, md: 0 }} color="##718197">
                <svg color="#718197" fill="#718197" stroke-width="0" version="1.1" viewBox="0 0 32 32" class="css-1djmjnl" height="1.8em" width="2.8em" xmlns="http://www.w3.org/2000/svg"><path d="M16 5.343c-6.196 0-11.219 5.023-11.219 11.219 0 4.957 3.214 9.162 7.673 10.645 0.561 0.103 0.766-0.244 0.766-0.54 0-0.267-0.010-1.152-0.016-2.088-3.12 0.678-3.779-1.323-3.779-1.323-0.511-1.296-1.246-1.641-1.246-1.641-1.020-0.696 0.077-0.682 0.077-0.682 1.126 0.078 1.72 1.156 1.72 1.156 1.001 1.715 2.627 1.219 3.265 0.931 0.102-0.723 0.392-1.219 0.712-1.498-2.49-0.283-5.11-1.246-5.11-5.545 0-1.226 0.438-2.225 1.154-3.011-0.114-0.285-0.501-1.426 0.111-2.97 0 0 0.941-0.301 3.085 1.15 0.894-0.25 1.854-0.373 2.807-0.377 0.953 0.004 1.913 0.129 2.809 0.379 2.14-1.453 3.083-1.15 3.083-1.15 0.613 1.545 0.227 2.685 0.112 2.969 0.719 0.785 1.153 1.785 1.153 3.011 0 4.31-2.624 5.259-5.123 5.537 0.404 0.348 0.761 1.030 0.761 2.076 0 1.5-0.015 2.709-0.015 3.079 0 0.299 0.204 0.648 0.772 0.538 4.455-1.486 7.666-5.69 7.666-10.645 0-6.195-5.023-11.219-11.219-11.219z"></path></svg>
            </Box>


            <Box display={{ sm: show ? "block" : "none", md: "block" }}
                mt={{ base: 4, md: 0 }}>
                <svg color="#718197" aria-hidden="true" focusable="false" height="1.8em" width="2.8em" viewBox="0 0 256 319"><defs><path d="M9.872 293.324L.012 30.574C-.315 21.895 6.338 14.54 15.005 14L238.494.032c8.822-.552 16.42 6.153 16.972 14.975.02.332.031.665.031.998v286.314c0 8.839-7.165 16.004-16.004 16.004-.24 0-.48-.005-.718-.016l-213.627-9.595c-8.32-.373-14.963-7.065-15.276-15.388z" id="IconifyId-16cf64ad71c-1b4c9-8"></path></defs><mask id="IconifyId-16cf64ad71c-1b4c9-9" fill="#fff"><use href="#IconifyId-16cf64ad71c-1b4c9-8"></use></mask><use fill="currentColor" href="#IconifyId-16cf64ad71c-1b4c9-8"></use><path d="M188.665 39.127l1.527-36.716L220.884 0l1.322 37.863a2.387 2.387 0 01-3.864 1.96l-11.835-9.325-14.013 10.63a2.387 2.387 0 01-3.829-2.001zm-39.251 80.853c0 6.227 41.942 3.243 47.572-1.131 0-42.402-22.752-64.684-64.415-64.684-41.662 0-65.005 22.628-65.005 56.57 0 59.117 79.78 60.249 79.78 92.494 0 9.052-4.433 14.426-14.184 14.426-12.705 0-17.729-6.49-17.138-28.552 0-4.786-48.458-6.278-49.936 0-3.762 53.466 29.548 68.887 67.665 68.887 36.935 0 65.892-19.687 65.892-55.326 0-63.36-80.961-61.663-80.961-93.06 0-12.728 9.455-14.425 15.07-14.425 5.909 0 16.546 1.042 15.66 24.801z" fill="#FFF" mask="url(#IconifyId-16cf64ad71c-1b4c9-9)"></path></svg>
            </Box>


            <Box display={{ sm: show ? "block" : "none", md: "block" }}
                mt={{ base: 4, md: 0 }}>
                <svg viewBox="0 0 24 24" color="#718197" focusable="false" role="presentation" width="2.5em" height="1.5em" aria-hidden="true" class="css-1im46kq"><g stroke-linejoin="full" stroke-linecap="full" stroke-width="2" fill="none" stroke="currentColor"><circle cx="12" cy="12" r="5"></circle><path d="M12 1v2"></path><path d="M12 21v2"></path><path d="M4.22 4.22l1.42 1.42"></path><path d="M18.36 18.36l1.42 1.42"></path><path d="M1 12h2"></path><path d="M21 12h2"></path><path d="M4.22 19.78l1.42-1.42"></path><path d="M18.36 5.64l1.42-1.42"></path></g></svg>
            </Box>
        </Flex>
    );
};

export default Header;
