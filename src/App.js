import React from "react";
import Header from "./header";
import Footer from "./footer"
import css from "./App.css"
import { theme, ThemeProvider, Text, Flex, Button, CSSReset, Box } from "@chakra-ui/core";

const breakpoints = ["360px", "768px", "1024px", "1440px"];
breakpoints.sm = breakpoints[0];
breakpoints.md = breakpoints[1];
breakpoints.lg = breakpoints[2];
breakpoints.xl = breakpoints[3];

const newTheme = {
  ...theme,
  breakpoints
};

class Accessible extends React.Component {
  render() {
    const Accessible = { title: "Accessible", description: "Chakra UI strictly follows WAI-ARIA standards. All components come with proper attributes and keyboard interactions out of the box." };
    return <div>
      <Box color="white" paddingRight="2em">
        <Text as="div" bg="#319795" w="3rem" h="3rem" borderRadius="9999px" paddingTop="0.5rem" paddingLeft="0.7rem">
          <svg stroke="#fff" fill="#fff" stroke-width="0" viewBox="0 0 24 24" class="css-yk8pw" height="2em" width="1.5em" xmlns="http://www.w3.org/2000/svg"><path d="M12 2c1.1 0 2 .9 2 2s-.9 2-2 2-2-.9-2-2 .9-2 2-2zm9 7h-6v13h-2v-6h-2v6H9V9H3V7h18v2z"></path></svg>
        </Text>
        <Text textAlign="left" fontSize="1.4rem" fontWeight="bold" paddingTop="1rem" paddingBottom="1rem">{Accessible.title}</Text>
        <Text maxW="350px" textAlign="left" lineHeight="23px">{Accessible.description}</Text>
      </Box>
    </div>;
  }
}

class Themeable extends React.Component {
  render() {
    const Themeable = { title: "Themeable", description: "Quickly and easily reference values from your theme throughout your entire application, on any component." };
    return <div>
      <Box color="white" paddingRight="1em">
        <Text as="div" bg="#319795" w="3rem" h="3rem" borderRadius="9999px" paddingTop="0.5rem" paddingLeft="0.7rem">
          <svg stroke="#fff" fill="#fff" stroke-width="0" viewBox="0 0 24 24" class="css-yk8pw" height="2em" width="1.5em" xmlns="http://www.w3.org/2000/svg"><path d="M12 3c-4.97 0-9 4.03-9 9s4.03 9 9 9c.83 0 1.5-.67 1.5-1.5 0-.39-.15-.74-.39-1.01-.23-.26-.38-.61-.38-.99 0-.83.67-1.5 1.5-1.5H16c2.76 0 5-2.24 5-5 0-4.42-4.03-8-9-8zm-5.5 9c-.83 0-1.5-.67-1.5-1.5S5.67 9 6.5 9 8 9.67 8 10.5 7.33 12 6.5 12zm3-4C8.67 8 8 7.33 8 6.5S8.67 5 9.5 5s1.5.67 1.5 1.5S10.33 8 9.5 8zm5 0c-.83 0-1.5-.67-1.5-1.5S13.67 5 14.5 5s1.5.67 1.5 1.5S15.33 8 14.5 8zm3 4c-.83 0-1.5-.67-1.5-1.5S16.67 9 17.5 9s1.5.67 1.5 1.5-.67 1.5-1.5 1.5z"></path></svg>
        </Text>
        <Text textAlign="left" fontSize="1.4rem" fontWeight="bold" paddingTop="1rem" paddingBottom="1rem">{Themeable.title}</Text>
        <Text textAlign="left" maxW="350px" lineHeight="23px">{Themeable.description}</Text>
      </Box>
    </div>;
  }
}

class Composable extends React.Component {
  render() {
    const Composable = { title: "Composable", description: "Components were built with composition in mind. You can leverage any component to create new things." };
    return <div>
      <Box color="white">
        <Text as="div" bg="#319795" w="3rem" h="3rem" borderRadius="9999px" paddingTop="0.5rem" paddingLeft="0.7rem">
          <svg stroke="#fff" fill="#fff" stroke-width="0" viewBox="0 0 24 24" class="css-yk8pw" height="2em" width="1.5em" xmlns="http://www.w3.org/2000/svg"><path d="M10 12c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zM6 8c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm0 8c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm12-8c1.1 0 2-.9 2-2s-.9-2-2-2-2 .9-2 2 .9 2 2 2zm-4 8c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm4-4c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-4-4c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2zm-4-4c-1.1 0-2 .9-2 2s.9 2 2 2 2-.9 2-2-.9-2-2-2z"></path></svg>
        </Text>
        <Text textAlign="left" as="h1" fontSize="1.4rem" fontWeight="bold" paddingTop="1rem" paddingBottom="1rem">{Composable.title}</Text>
        <Text textAlign="left" maxW="350px" lineHeight="23px">{Composable.description}</Text>
      </Box>
    </div>;
  }
}

function App() {
  return (
    <ThemeProvider theme={newTheme}>
      <CSSReset />
      <Box bg="rgb(26, 32, 44)">
        <Header />
        <Flex alignContent="center" justifyContent="center" align="center" textAlign="center" paddingTop="4em" paddingBottom="9rem" align="center">
          <Box>
            <Text color="white" textAlign="center">
              <Text as="p" fontSize="40px" fontWeight="500">Build accessible React apps & <br /> websites <Text color="#319795" as="span">with speed</Text></Text>
              <Text as="p" fontSize="20px" color="#A1A1A1">Chakra UI is a simple, modular and accessible component library <br /> that gives you all the building blocks you need to build your React <br /> applications.</Text>
              <Button bg="#4FD1C5" color="black" padding="1.5em" marginTop="2rem" marginRight="1em"
                borderRadius="5px">
                Get Started
          </Button>
              <Button bg="#3F454E" color="white" padding="1.5em" marginTop="2rem" borderRadius="5px">
                <svg stroke="currentColor" fill="currentColor" stroke-width="0" version="1.1" viewBox="0 0 32 32" data-custom-icon="true" focusable="false" aria-hidden="true" class="css-1pmpdjw" height="1.5em" width="1.5em" xmlns="http://www.w3.org/2000/svg"><path d="M16 5.343c-6.196 0-11.219 5.023-11.219 11.219 0 4.957 3.214 9.162 7.673 10.645 0.561 0.103 0.766-0.244 0.766-0.54 0-0.267-0.010-1.152-0.016-2.088-3.12 0.678-3.779-1.323-3.779-1.323-0.511-1.296-1.246-1.641-1.246-1.641-1.020-0.696 0.077-0.682 0.077-0.682 1.126 0.078 1.72 1.156 1.72 1.156 1.001 1.715 2.627 1.219 3.265 0.931 0.102-0.723 0.392-1.219 0.712-1.498-2.49-0.283-5.11-1.246-5.11-5.545 0-1.226 0.438-2.225 1.154-3.011-0.114-0.285-0.501-1.426 0.111-2.97 0 0 0.941-0.301 3.085 1.15 0.894-0.25 1.854-0.373 2.807-0.377 0.953 0.004 1.913 0.129 2.809 0.379 2.14-1.453 3.083-1.15 3.083-1.15 0.613 1.545 0.227 2.685 0.112 2.969 0.719 0.785 1.153 1.785 1.153 3.011 0 4.31-2.624 5.259-5.123 5.537 0.404 0.348 0.761 1.030 0.761 2.076 0 1.5-0.015 2.709-0.015 3.079 0 0.299 0.204 0.648 0.772 0.538 4.455-1.486 7.666-5.69 7.666-10.645 0-6.195-5.023-11.219-11.219-11.219z"></path></svg>
          Github
          </Button>
            </Text>
          </Box>
        </Flex>

        <Flex direction={['column', 'column', 'row', 'row']} paddingTop="3rem" paddingBottom="3rem" alignContent="center" justifyContent="center" align="center" textAlign="center" borderTop="1px" borderBottom="1px" color="#303741">
          <Accessible></Accessible>
          <Themeable></Themeable>
          <Composable></Composable>
        </Flex>

        <Footer />
      </Box>
    </ThemeProvider >
  );
}

export default App;
